let option1 = document.querySelector (".option1");
let option2 = document.querySelector (".option2");
let option3 = document.querySelector (".option3");
let audio = document.querySelector (".audio");

let answer = 0;
document.querySelector (".add").addEventListener ("click" , generate_equation ());

function generate_equation () {

    let num1 = Math.floor (Math.random () * 13);
    let num2 = Math.floor (Math.random () * 13);
    let dummyAnswer1 = Math.floor (Math.random () * 10);
    let dummyAnswer2 = Math.floor (Math.random () * 10);
    let allAnswer = [];
    let switchAnswer = [];

    
    if (num1 > num2) {
        answer = eval (num1 - num2);
        document.querySelector (".num1").innerHTML = num1;
        document.querySelector (".num2").innerHTML = num2;
    } else {
        answer = eval (num2 - num1);
        document.querySelector (".num1").innerHTML = num2;
        document.querySelector (".num2").innerHTML = num1;
    }

    allAnswer = [answer, dummyAnswer1, dummyAnswer2];

    for (i = allAnswer.length; i--;) {
        switchAnswer.push (allAnswer.splice(Math.floor(Math.random () *(i + 1)),1)[0])
    }

    option1.innerHTML = switchAnswer[0];
    option2.innerHTML = switchAnswer[1];
    option3.innerHTML = switchAnswer[2];
}

option1.addEventListener ("click", () => {
    if (option1.innerHTML == answer) {
        generate_equation ();
    } else {
        audio.play()
    }
});
option2.addEventListener ("click", () => {
    if (option2.innerHTML == answer) {
        generate_equation ();
    } else {
        audio.play()
    }
});
option3.addEventListener ("click", () => {
    if (option3.innerHTML == answer) {
        generate_equation ();
    } else {
        audio.play()
    }
});

generate_equation ();